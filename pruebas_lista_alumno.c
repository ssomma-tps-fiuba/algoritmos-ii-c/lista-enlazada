#include "lista.h"
#include "testing.h"
#include <stdio.h>
#include <stdlib.h>


void prueba_crear(){
	lista_t* lista = lista_crear();
	print_test("lista creada", lista != NULL);
	lista_destruir(lista, NULL);
}

void pruebas_lista_vacia(){
	lista_t* lista = lista_crear();
	print_test("Ver primero y ultimo son invalidos en lista vacia", lista_esta_vacia(lista) && !lista_ver_primero(lista) && !lista_ver_ultimo(lista));
	print_test("Borrar primero invalido en lista vacia", lista_esta_vacia(lista) && !lista_borrar_primero(lista));
	lista_destruir(lista, NULL);
}

void pruebas_insertar_primero(){
	lista_t* lista = lista_crear();
	int dato1 = 64;
	int dato2 = 128;
	int dato3 = 256;
	printf("/***INICIO DE PRUEBAS INSERTAR PRIMERO***/\n");
	print_test("Inserto un dato con al principio", lista_insertar_primero(lista, &dato1));
	print_test("La pila no esta vacia y el dato insertado se ve como primero y ultimo", !lista_esta_vacia(lista) && lista_ver_primero(lista) == &dato1 && lista_ver_ultimo(lista) == &dato1);
	
	lista_insertar_primero(lista, &dato2);
	print_test("Inserto otro dato como primero, el dato anterior se ve como ultimo", lista_ver_primero(lista) == &dato2 && lista_ver_ultimo(lista) == &dato1);
	
	lista_insertar_primero(lista, &dato3);
	print_test("Inserto otro dato como primero y el primero insertado se ve como ultimo", lista_ver_primero(lista) == &dato3 && lista_ver_ultimo(lista) == &dato1);
	print_test("El dato insertado segundo no lo veo", lista_ver_primero(lista) != &dato2 && lista_ver_ultimo(lista) != &dato2);
	
	int* primero = lista_ver_primero(lista);
	int* ultimo = lista_ver_ultimo(lista);
	print_test("No se modifico el valor del dato al insertarlo", *primero == dato3 && *ultimo == dato1 && *primero != dato2 && *primero != dato2);
	lista_destruir(lista, NULL);
	printf("/***FIN DE PRUEBAS INSERTAR PRIMERO***/\n");
}

void pruebas_insertar_ultimo(){
	lista_t* lista = lista_crear();
	int dato1 = 512;
	int dato2 = 1024;
	int dato3 = 2048;
	printf("/***INICIO DE PRUEBAS INSERTAR ULTIMO***/\n");
	print_test("Inserto un dato al final", lista_insertar_ultimo(lista, &dato1));
	print_test("La pila no esta vacia y el dato insertado se ve como primero y ultimo", !lista_esta_vacia(lista) && lista_ver_primero(lista) == &dato1 && lista_ver_ultimo(lista) == &dato1);
	
	lista_insertar_ultimo(lista, &dato2);
	print_test("Inserto otro dato como ultimo, el dato anterior se ve como primero", lista_ver_primero(lista) == &dato1 && lista_ver_ultimo(lista) == &dato2);
	
	lista_insertar_ultimo(lista, &dato3);
	print_test("Inserto otro dato como ultimo y el primero insertado se ve como primero", lista_ver_primero(lista) == &dato1 && lista_ver_ultimo(lista) == &dato3);
	print_test("El dato insertado segundo no lo veo", lista_ver_primero(lista) != &dato2 && lista_ver_ultimo(lista) != &dato2);
	
	int* primero = lista_ver_primero(lista);
	int* ultimo = lista_ver_ultimo(lista);
	print_test("No se modifico el valor del dato al insertarlo", *primero == dato1 && *ultimo == dato3 && *primero != dato2 && *primero != dato2);
	lista_destruir(lista, NULL);
	printf("/***FIN DE PRUEBAS INSERTAR ULTIMO***/\n");
}

void prueba_insertar_NULL(){
	lista_t* lista = lista_crear();
	print_test("Puedo insertar un NULL y la lista no va a estar vacia", lista_insertar_primero(lista, NULL) && lista_ver_primero(lista) == NULL && !lista_esta_vacia(lista));
	lista_destruir(lista, NULL);
}

void pruebas_lista_alumno(){

	lista_t* ejemplo = NULL;
	print_test("Puntero inicializado a NULL", ejemplo == NULL);
	
	prueba_crear();
	pruebas_lista_vacia();
	pruebas_insertar_primero();
	pruebas_insertar_ultimo();
	prueba_insertar_NULL();

}