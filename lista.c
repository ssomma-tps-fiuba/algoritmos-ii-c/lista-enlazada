#include "lista.h"
#include <stdbool.h>
#include <stdlib.h>

typedef struct nodo nodo_t;

struct nodo {
	void* dato;
	nodo_t* prox;
};

struct lista {
	nodo_t* prim;
	nodo_t* ult;
	size_t tam;
};


// Primitivas de Nodo

nodo_t* nodo_crear(void* valor){ 

	nodo_t* nodo = malloc(sizeof(nodo_t));
	
	if(nodo == NULL){
		return NULL;
	}

	nodo->dato = valor;
	nodo->prox = NULL;

	return nodo;

}

void* nodo_destruir(nodo_t* nodo){
	void* dato_nodo = nodo->dato; 
	free(nodo);
	return dato_nodo;
}

// Primitivas de Lista
lista_t* lista_crear(void){

	lista_t* lista = malloc(sizeof(lista_t));
	if(lista == NULL){
		return NULL;
	}

	lista->prim = NULL;
	lista->ult = NULL;
	lista->tam = 0;
	return lista;

}


bool lista_esta_vacia(const lista_t* lista){
	return lista->tam == 0;
}


size_t lista_largo(const lista_t* lista){
	return lista->tam;
}


void* lista_ver_primero(const lista_t* lista){
	return (lista_esta_vacia(lista)) ? NULL : lista->prim->dato;
}


void* lista_ver_ultimo(const lista_t* lista){
	return (lista_esta_vacia(lista)) ? NULL : lista->ult->dato;
}


bool lista_insertar_primero(lista_t* lista, void* dato){
	
	nodo_t* nodo = nodo_crear(dato);
	if(nodo == NULL){
		return false;
	}
	
	if(lista_esta_vacia(lista)){
		lista->ult = nodo;
	}
	else{
		nodo_t* primero_actual = lista->prim;
		nodo->prox = primero_actual;
	}

	lista->prim = nodo;
	lista->tam++;

	return true;
}


bool lista_insertar_ultimo(lista_t* lista, void* dato){
	
	nodo_t* nodo = nodo_crear(dato);
	if(nodo == NULL){
		return false;
	}
	
	if(lista_esta_vacia(lista)){
		lista->prim = nodo;
	}
	else{
		nodo_t* ultimo_actual = lista->ult;
		ultimo_actual->prox = nodo;
	}

	lista->ult = nodo;
	lista->tam++;

	return true;
}


void* lista_borrar_primero(lista_t* lista){

	if(lista_esta_vacia(lista)){
		return NULL;
	}

	nodo_t* primero_actual = lista->prim;
	
	lista->prim = lista->prim->prox;
	
	lista->tam--;

	return nodo_destruir(primero_actual);

}

void lista_destruir(lista_t* lista, void destruir_dato(void*)){ 

	void* dato_actual;
	
	while(!lista_esta_vacia(lista)){
		
		dato_actual = lista_borrar_primero(lista);
		  
		if(destruir_dato != NULL){
			destruir_dato(dato_actual);
		}
	}

	free(lista);
}

/* iterador interno */ 

void lista_iterar(lista_t *lista, bool visitar(void *dato, void *extra), void *extra){
	
	bool visitado = true;
	void* actual_dato;
	nodo_t* actual = lista->prim;
	
	while(actual != NULL || visitado){

		actual_dato = actual->dato;
		visitar(actual_dato, extra);
		actual = actual->prox;

	}

}


// iterador externo

struct lista_iter{

	lista_t* lista;
	nodo_t* anterior;
	nodo_t* actual;

};


lista_iter_t* lista_iter_crear(lista_t *lista){

	lista_iter_t* iter = malloc(sizeof(lista_iter_t));
	if(iter == NULL){
		return NULL;
	}

	iter->lista = lista;
	iter->anterior = NULL;
	iter->actual = lista->prim;

	return iter;

}


bool lista_iter_avanzar(lista_iter_t *iter){

	if(lista_iter_al_final(iter)){
		return false;
	}
	
	iter->anterior = iter->actual;
	iter->actual = iter->actual->prox;
	return true;

}


void* lista_iter_ver_actual(const lista_iter_t *iter){
	return (iter->actual == NULL) ? NULL : iter->actual->dato;
}


bool lista_iter_al_final(const lista_iter_t *iter){
	return lista_esta_vacia(iter->lista) || iter->actual == NULL;
}


void lista_iter_destruir(lista_iter_t *iter){
	free(iter);
}


bool lista_iter_insertar(lista_iter_t *iter, void *dato){
	
	nodo_t* insertar = nodo_crear(dato);
	if(insertar == NULL){
		return false;
	}

	if(lista_esta_vacia(iter->lista)){
		iter->lista->prim = insertar;
		iter->lista->ult = insertar;
		iter->actual = insertar;
		iter->lista->tam++;
		return true;
	}
	
	if(iter->anterior != NULL){
		iter->anterior->prox = insertar;
	}

	
	insertar->prox = iter->actual;
	iter->lista->prim = (iter->lista->prim == iter->actual) ? insertar : iter->lista->prim;
	iter->actual = insertar;
	
	iter->lista->tam++;

	return true;

}

void* lista_iter_borrar(lista_iter_t *iter){

	if(lista_esta_vacia(iter->lista)){
		return NULL;
	}

	nodo_t* borrar = iter->actual;
	
	if(iter->anterior != NULL){
		iter->anterior->prox = borrar->prox;
	}

	iter->actual = borrar->prox;
	iter->lista->tam--;
	
	return nodo_destruir(borrar);	

}